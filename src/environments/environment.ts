// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyC2MRaj1zZsXG9oJ3LQ3Ub7oWuZGfxSt8w',
    authDomain: 'ion-market-1.firebaseapp.com',
    projectId: 'ion-market-1',
    storageBucket: 'ion-market-1.appspot.com',
    messagingSenderId: '734466172528',
    appId: '1:734466172528:web:97a2c23c78a1e4b44936b5',
    measurementId: 'G-2QY5PNWVY7'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
