import { Component, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';
import { DataService } from '../../service/Data.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

  items = [];

  constructor(
    private dataService: DataService
  ) { }

  ngOnInit() {
  }

  onSearchChange(evt?: any) {
    const key: string = evt.target.value;
    const lowerCase = key.toLowerCase();
    if (lowerCase.length) {
      this.dataService.search(lowerCase).pipe(take(1)).subscribe(items => {
        this.items = [];
        this.items.push(...items);
      });
    }
    else {
      this.items = [];
    }
  }

}
