import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Users } from 'src/app/interfaces/users';
import { AuthService } from '../../service/auth.service';
import { NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;

  constructor(
    private navCtrl: NavController,
    private fb: FormBuilder,
    private auth: AuthService,
    private storage: Storage,
    private toastCtrl: ToastController

  ) {
    this.createForm();
  }


  ngOnInit() {
  }


  createForm() {
    this.loginForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
  onSubmit() {
    const data: Users = this.loginForm.value;
    this.auth.loginWithEmail(data.email, data.password)
      .then(async () => {
        console.log(this.storage);
        const toast = await this.toastCtrl.create({
          message: 'login successed',
          duration: 2000
        });
        await toast.present();
        this.navCtrl.navigateForward('/menu/pages/home');
      })
      .catch(async err => {
        const toast = await this.toastCtrl.create({
          message: err,
          duration: 2000
        });
        await toast.present();
        console.log(err);
      });
  }

  navToSign() {
    this.navCtrl.navigateForward('signup');
  }

  loginFacebook() {

    this.auth.signFaceBook();


    this.navCtrl.navigateForward('/menu/pages/home');




  }


}
