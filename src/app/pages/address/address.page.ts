import { Component, OnInit, OnDestroy } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CartService } from '../../service/cart.service';
import { Address } from '../../interfaces/address';
import { DataService } from '../../service/Data.service';

@Component({
  selector: 'app-modal-address',
  templateUrl: './address.page.html',
  styleUrls: ['./address.page.scss'],
})
export class ModalAddressPage implements OnInit, OnDestroy {

  addressForm: FormGroup;
  address: Address;
  index: number;

  constructor(
    private modal: ModalController,
    private fb: FormBuilder,
    private cartService: CartService,
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.creatForm();

    this.index = this.dataService.getparams()[0]?.index;
    this.address = this.dataService.getparams()[0]?.address;

    this.patchValue();


  }

  patchValue() {
    if (this.address) {
      this.addressForm.patchValue({
        country: this.address.country,
        city: this.address.city,
        state: this.address.state,
        address: this.address.address,
        pincode: this.address.pincode
      });
    }
  }

  dissmis() {
    this.modal.dismiss(() => {
      this.cartService.reloadDataStorage('address');
    });
    this.dataService.setparams({});
  }

  creatForm() {
    this.addressForm = this.fb.group({
      country: ['', Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required],
      address: ['', Validators.required],
      pincode: ['', Validators.required]
    });
  }

  save() {
    const addressData = {
      country: this.addressForm.value.country,
      state: this.addressForm.value.state,
      city: this.addressForm.value.city,
      address: this.addressForm.value.address,
      pincode: this.addressForm.value.pincode
    };
    if (this.address) {
      this.cartService.updateAddress(addressData, this.index);
    } else {
      this.cartService.addAddress(addressData);
    }
    this.modal.dismiss(() => {
      this.cartService.reloadDataStorage('address');
    });
  }

  ngOnDestroy(): void {
    this.dataService.setparams({});
  }




}
