import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalAddressPageRoutingModule } from './address-routing.module';

import { ModalAddressPage } from './address.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ModalAddressPageRoutingModule
  ],
  declarations: [ModalAddressPage]
})
export class ModalAddressPageModule { }
