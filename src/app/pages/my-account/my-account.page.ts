import { Component, OnChanges, OnInit } from '@angular/core';
import { ModalController, NavController, AlertController } from '@ionic/angular';
import { AuthService } from '../../service/auth.service';
import { ModalAddressPage } from '../address/address.page';
import { CartService } from '../../service/cart.service';
import { Address } from '../../interfaces/address';
import { DataService } from '../../service/Data.service';

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.page.html',
  styleUrls: ['./my-account.page.scss'],
})
export class MyAccountPage implements OnInit {


  userData: any;
  sellectedSegment = 'Address';
  address = [];
  cartItemCount: number;
  favItemCount: number;

  constructor(
    private authService: AuthService,
    private modalCtrl: ModalController,
    private cartService: CartService,
    private navCtrl: NavController,
    private alertCtrl: AlertController,
    private dataService: DataService
  ) { }


  async ngOnInit() {

    // get user data
    this.getUserDate();

    // get address
    await this.cartService.reloadDataStorage('address');
    this.address = this.cartService.address;

    // get cart count
    this.cartService.getCartCount().subscribe((data) => {
      this.cartItemCount = data;
    });

    // get Favorite count
    this.cartService.getFavCount().subscribe((data) => {
      this.favItemCount = data;
    });
  }



  getUserDate() {
    this.authService.iuser.subscribe(user => {
      this.authService.userId = user.uid;
      this.userData = user;
    });
  }

  openCart() {
    this.navCtrl.navigateForward('cart');
  }

  openFav() {
    this.navCtrl.navigateForward('wish-list');
  }


  /**************     [segmant]   ***********/

  segmentChanged(ev) {
    this.sellectedSegment = ev.target.value;
  }

  async addAddress() {
    const modal = await this.modalCtrl.create({
      component: ModalAddressPage,
    });
    await modal.present();

  }

  async deleteAddress(index: number) {
    const alert = await this.alertCtrl.create({
      message: 'Are you sure for deleteing ?',
      mode: 'ios',
      buttons: [
        {
          text: 'cancel',
          role: 'cancel'
        },
        {
          text: 'Ok',
          handler: () => {
            this.cartService.deleteAddress(index);
          }
        }
      ]
    });
    await alert.present();
  }

  editAddress(address: Address, index: number) {
    this.dataService.setparams({
      index,
      address
    });
    this.addAddress();
  }



}
