import { Component, OnInit } from '@angular/core';
import { ActionSheetController, ModalController, NavController } from '@ionic/angular';
import { ModalAddressPage } from '../address/address.page';
import { CartService } from '../../service/cart.service';
import { Address } from '../../interfaces/address';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.page.html',
  styleUrls: ['./confirm.page.scss'],
})
export class ConfirmPage implements OnInit {

  constructor(
    private actionSheet: ActionSheetController,
    private modalCtrl: ModalController,
    private navCtrl: NavController,
    private cartService: CartService
  ) { }
  addressLength: number;
  mainAddress: Address;
  zone: string;
  step = 1;
  address = [];

  ngOnInit() {
    this.address = this.cartService.address;
  }



  next() {
    this.step += 1;
  }

  previous() {
    this.step -= 1;
  }

  createButtons() {
    const buttons = [];

    for (let i = 0; i < this.address.length; i++) {

      const button = {
        text: this.address[i].address,
        icon: 'location',
        handler: () => {
          this.mainAddress = this.address[i];
        },
      };
      buttons.push(button);
    }
    return buttons;
  }

  async sellectAddress() {

    const action = await this.actionSheet.create({
      mode: 'ios',
      header: 'sellect address',
      buttons: [
        ...this.createButtons(),
        {
          text: 'Add New Address',
          icon: 'add',
          handler: async () => {
            const modal = await this.modalCtrl.create({
              component: ModalAddressPage
            });
            await modal.present().then(_ => {
              this.addressLength = this.address.length;
            });
            await modal
              .onDidDismiss().then(_ => {
                if (this.addressLength < this.address.length) {
                  this.mainAddress = this.address[this.address.length - 1];
                }
              });
          }
        },
        {
          text: 'cancel',
          icon: 'close-outline',
          role: 'cancel'
        }
      ]
    });

    await action.present();
  }

  orderNow() {

  }



  close() {
    this.navCtrl.navigateBack('cart');
  }

}
