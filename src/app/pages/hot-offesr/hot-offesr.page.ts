import { Component, OnInit } from '@angular/core';

import { Product } from 'src/app/interfaces/product';
import { DataService } from '../../service/Data.service';
import { CartService } from '../../service/cart.service';
import { NavController } from '@ionic/angular';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-hot-offesr',
  templateUrl: './hot-offesr.page.html',
  styleUrls: ['./hot-offesr.page.scss'],
})
export class HotOffesrPage implements OnInit {


  products = [];

  constructor(
    private dataService: DataService,
    private cartService: CartService,
    private navCtrl: NavController
  ) { }

  ngOnInit() {
    this.getDiscountProduct();
  }

  getDiscountProduct() {
    this.dataService.getDiscountProduct().pipe(take(1)).subscribe(data => {
      this.products.push(...data);
    });
  }


  toggleFav(pro: Product) {
    this.cartService.toggleFavorite(pro);
    if (pro.favorite) {
      pro.favorite = false;
    } else {
      pro.favorite = true;
    }
  }

  detail(product: Product) {
    this.dataService.setparams({});
    this.dataService.setparams(product);
    this.navCtrl.navigateForward(`detail/${product.id}`);
  }


  track(index: number, pro: Product) {
    return pro.id;
  }

}
