import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HotOffesrPageRoutingModule } from './hot-offesr-routing.module';

import { HotOffesrPage } from './hot-offesr.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HotOffesrPageRoutingModule
  ],
  declarations: [HotOffesrPage]
})
export class HotOffesrPageModule {}
