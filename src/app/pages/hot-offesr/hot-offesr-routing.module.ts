import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HotOffesrPage } from './hot-offesr.page';

const routes: Routes = [
  {
    path: '',
    component: HotOffesrPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HotOffesrPageRoutingModule {}
