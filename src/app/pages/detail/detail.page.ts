import { identifierModuleUrl } from '@angular/compiler';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Product } from 'src/app/interfaces/product';
import { DataService } from '../../service/Data.service';
import { take } from 'rxjs/operators';
import { NavController, ToastController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { CartService } from '../../service/cart.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit, OnDestroy {

  products: Product[] = [];
  sameProducts: Product[] = [];
  cartItemCount: number;

  constructor(
    private dataService: DataService,
    private navCtrl: NavController,
    private route: ActivatedRoute,
    private cartService: CartService,
    private toastCtrl: ToastController,
    private sharing: SocialSharing
  ) { }

  ngOnInit() {
    this.products = this.dataService.getparams();

    // get others products that like this
    this.getSimilarProduct();
    this.products = this.dataService.getparams();

    // get cart
    this.cartService.getCartCount().subscribe(count => {
      this.cartItemCount = count;
    });

  }


  getSimilarProduct() {
    this.dataService
      .getProductWithSebCategory(this.products[0].subId)
      .pipe(take(1)).subscribe((data) => {
        this.sameProducts = data;
      });
  }

  setViewId(id: any) {
    this.dataService.setparams([id]);
    this.navCtrl.navigateForward('view-categories/' + false);
  }

  detail(pro: Product) {
    this.dataService.setparams({});
    this.dataService.setparams(pro);
    this.products[0] = pro;
  }

  ngOnDestroy() {
    this.dataService.setparams({});
  }

  addToCart(pro: Product) {
    this.cartService.addProduct(pro)
      .then(async _ => {
        const toast = await this.toastCtrl.create({
          message: 'this product added to cart successfully',
          mode: 'ios',
          color: 'dark',
          duration: 2000
        });
        await toast.present();
      }).catch(async e => {
        const toast = await this.toastCtrl.create({
          message: e.message,
          mode: 'ios',
          color: 'danger',
          duration: 2000,
        });
        await toast.present();
      });
  }

  openCart() {
    this.navCtrl.navigateForward('cart');
  }

  addFav(pro: Product) {
    this.cartService.toggleFavorite(pro);
    if (pro.favorite) {
      pro.favorite = false;
    } else {
      pro.favorite = true;
    }
  }

  /******************     [Social Sharing]    *****************/
  shareWhats() {
    this.sharing
      .share(
        this.products[0].description,
        this.products[0].imgUrl
      )
      .catch(err => alert(err));
  }

  shareFacebook() {

  }

  shareTwitter() {
    this.sharing
      .shareViaTwitter(
        this.products[0].description,
        this.products[0].imgUrl
      )
      .catch(err => alert(err));
  }

  shareInsta() {
    this.sharing.shareViaInstagram(
      this.products[0].description,
      this.products[0].imgUrl
    )
      .catch(err => alert(err));
  }


}
