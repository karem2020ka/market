import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from '../../service/Data.service';
import { Product } from '../../interfaces/product';
import { Category } from '../../interfaces/category';
import { Subcategory } from '../../interfaces/subcategory';
import { take } from 'rxjs/operators';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { CartService } from '../../service/cart.service';


@Component({
  selector: 'app-view-categories',
  templateUrl: './view-categories.page.html',
  styleUrls: ['./view-categories.page.scss'],
})
export class ViewCategoriesPage implements OnInit, OnDestroy {

  constructor(
    private dataService: DataService,
    private navCtrl: NavController,
    private route: ActivatedRoute,
    private cartService: CartService
  ) { }


  all: boolean;
  products: Product[] = [];
  data: Category;
  sub: Subcategory[] = [];
  sellectedSub = 'all';
  catId: string;
  subId: string;


  ngOnInit() {
    this.getDataToViewCategories();
  }


  getDataToViewCategories() {

    const fromHome = this.route.snapshot.paramMap.get('fromHome') === 'true';

    this.data = this.dataService.getparams()[0];
    if (fromHome) {
      this.all = true;
      this.dataService.getSubCategory(this.data[0].id)
        .pipe(take(1))
        .subscribe((data) => {
          this.sub = data;
        });
      this.getProductWithCategory(this.data[0].id);
      this.catId = this.data[0].id;
    }
    else {
      this.getProductWithSebCategory(this.data[0]);

    }
  }


  getProductWithSebCategory(id: string) {
    this.sellectedSub = id;
    this.dataService.getProductWithSebCategory(id)
      .pipe(take(1)).subscribe((data) => {
        this.products = data;
        this.subId = id;
        this.check();
      });
  }

  getProductWithCategory(id: any) {
    this.sellectedSub = 'all';
    this.dataService.getProductWithCategory(this.data[0].id)
      .pipe(take(1))
      .subscribe((data) => {
        this.products = data;
        this.catId = id;
        this.check();
      });
  }


  detail(pro: any) {
    this.dataService.setparams(pro);
    this.navCtrl.navigateForward(`detail/${pro.id}`);
  }

  ngOnDestroy() {
    // this.dataService.setProducts({});
  }


  addFav(pro: Product) {
    this.cartService.toggleFavorite(pro);
    if (pro.favorite) {
      pro.favorite = false;
    } else {
      pro.favorite = true;
    }
  }

  check() {
    this.products.forEach(pro => {
      this.cartService.fav.forEach(fav => {
        if (pro.id === fav.id) {
          pro.favorite = true;
        }
      });
    });
  }


  /**************************   [ Pagination ]   **************************/

  paginationCategory(ev?: any) {
    this.dataService.paginationCategory(
      this.products[this.products.length - 1].name
      , this.catId).pipe(take(1)).subscribe(data => {
        this.products.push(...data);
      });
    ev.target.complete();
  }

  paginationSubCategory(ev?: any) {
    this.dataService.paginationSubCategory(
      this.products[this.products.length - 1].name
      , this.data[0]).pipe(take(1)).subscribe(data => {
        this.products.push(...data);
      });
    ev.target.complete();
  }

  loadData(ev?: any) {
    if (this.sellectedSub === 'all') {
      this.paginationCategory(ev);
    } else {
      this.paginationSubCategory(ev);
    }
  }

  track(index: number, pro: Product) {
    return pro.id;
  }

}
