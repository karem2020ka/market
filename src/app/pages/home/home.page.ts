import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { DataService } from '../../service/Data.service';
import { Product } from './../../interfaces/product';
import { AngularFirestore } from '@angular/fire/firestore';
import { Category } from '../../interfaces/category';
import { Subcategory } from '../../interfaces/subcategory';
import { take } from 'rxjs/operators';
import { Homeprod } from '../../interfaces/homeprod';
import { CartService } from '../../service/cart.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})


export class HomePage implements OnInit {


  cartItemCount: number;
  favItemCount: number;
  category: Category[] = [];
  subCat: Subcategory[] = [];
  product: Product[] = [];
  slidproducts: Homeprod[] = [];
  loading: boolean;
  err: string;


  ////////////// sliders
  slideOpts1 = {
    initialSlide: 0,
    direction: 'horizontal',
    speed: 300,
    spaceBetween: 2,
    slidesPerView: 4.2,
    freeMode: true,
    loop: false
  };

  slideOpts2 = {
    initialSlide: 0,
    direction: 'horizontal',
    speed: 600,
    spaceBetween: 0,
    slidesPerView: 1,
    freeMode: false,
    loop: true,
    autoplay: true
  };

  constructor(
    private dataService: DataService,
    private navCtrl: NavController,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private cartService: CartService
  ) { }


  ngOnInit() {
    this.getData();
    this.getCartItemCount();

    this.getProductCategory();

    this.getFavItemCount();
  }


  openCart() {
    this.navCtrl.navigateForward('cart');
  }

  openFav() {
    this.navCtrl.navigateForward('wish-list');
  }


  detail(product: Product) {
    this.dataService.setparams({});
    this.dataService.setparams(product);
    this.navCtrl.navigateForward(`detail/${product.id}`);
  }

  getData(ev?: any) {
    (ev) ? ev.target.complete() : this.loading = true;
    // get a category to slider
    return this.dataService.getCategory()
      .pipe(take(1))
      .subscribe((data) => {
        // get  categor
        this.category = data;
        // get product with category
        // this.getProductCategory();
      }, async er => {
        console.log(er);
        const toast = await this.toastCtrl.create({ message: er.message });
        toast.present();
      }
      );

  }

  getProductCategory(ev?: any) {
    this.category.forEach((category) => {
      if (category.sub === true) {
        const slideCategory: Homeprod = {
          category,
          products: []
        };
        // this.dataService.limit = 4;
        return this.dataService.getProductWithCategory(category.id)
          .pipe(take(1))
          .subscribe((data) => {
            if (this.slidproducts.length < 2) {
              this.slidproducts.push(slideCategory);
            }
            for (let i = 0; i < this.product.length; i++) {
              //   this.product.push(data[i]);
              slideCategory.products.push(data[i])

            }

            this.checkFavorite();
          }, async er => {
            console.log(er);
            const toast = await this.toastCtrl.create({ message: er.message });
            toast.present();
          });
      }
      ev ? ev.target.complete() : this.loading = false;
    });
  }

  doRefresh(ev: any) {
    this.getData(ev);
    this.getProductCategory(ev);
  }

  getCat(cat: any) {
    // this.dataService.limit = 1000;
    this.dataService.setparams([cat]);
    this.navCtrl.navigateForward('view-categories/' + true);
  }

  async getCartItemCount() {
    // reload cart items
    await this.cartService.reloadDataStorage('cart');
    const cart = this.cartService.cart;
    let total = 0;
    cart.forEach((el) => {
      total = total + el.amount;
    });
    this.cartService.cartItemCount.next(total);
    this.cartService.getCartCount().subscribe((data) => {
      this.cartItemCount = data;
    }, async er => {
      console.log(er);
      const toast = await this.toastCtrl.create({ message: er.message });
      toast.present();
    });

  }
  async getFavItemCount() {
    // reload favorite items
    await this.cartService.reloadDataStorage('fav');
    const totalFav = this.cartService.fav.length;
    this.cartService.favCount.next(totalFav);
    this.cartService.getFavCount().subscribe((data) => {
      this.favItemCount = data;
    }, async er => {
      console.log(er);
      const toast = await this.toastCtrl.create({ message: er.message });
      toast.present();
    });
  }

  addFav(pro: Product) {
    this.cartService.toggleFavorite(pro);
    if (pro.favorite) {
      pro.favorite = false;
    } else {
      pro.favorite = true;
    }
  }

  checkFavorite() {
    this.product.forEach(pro => {
      this.cartService.fav.forEach(fav => {
        if (pro.id === fav.id) {
          pro.favorite = true;
        }
      });
    });
  }


  track(index: number, product: Product) {
    return product.id;
  }


}
