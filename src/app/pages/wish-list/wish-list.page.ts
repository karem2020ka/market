import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/interfaces/product';
import { CartService } from '../../service/cart.service';
import { DataService } from '../../service/Data.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-wish-list',
  templateUrl: './wish-list.page.html',
  styleUrls: ['./wish-list.page.scss'],
})
export class WishListPage implements OnInit {

  constructor(
    private cartService: CartService,
    private dataService: DataService,
    private navCtrl: NavController
  ) { }

  fav: Product[] = [];

  ngOnInit() {
    this.fav = this.cartService.getFavorite();
  }


  detail(product: Product) {
    this.dataService.setparams({});
    this.dataService.setparams(product);
    this.navCtrl.navigateForward(`detail/${product.id}`);
  }

  toggleFav(pro: Product) {
    this.cartService.toggleFavorite(pro);
  }

  track(index: number, pro: Product) {
    return pro.id;
  }


}
