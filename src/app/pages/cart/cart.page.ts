import { Component, OnInit } from '@angular/core';
import { CartService } from '../../service/cart.service';
import { Product } from 'src/app/interfaces/product';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { AuthService } from '../../service/auth.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {


  cart: Product[] = [];
  // cartItemCount: number;
  totalCash: number;
  totalAmount: number;
  skip: number;

  constructor(
    private cartService: CartService,
    private alertCtrl: AlertController,
    private navCtrl: NavController,
    private authService: AuthService,
    private toastCtrl: ToastController) { }

  ngOnInit() {
    this.cart = this.cartService.getCart();
    this.getTotal();
    // this.cartService.reloadCart();

  }

  increasProduct(pro: Product) {
    this.cartService.addProduct(pro);
    this.getTotal();

  }

  async decreaseProduct(pro: Product) {
    const alert = await this.alertCtrl.create({
      message: 'are you sure want remove this product',
      mode: 'ios',
      buttons: [{
        text: 'yes',
        handler: () => {
          this.cartService.decreaseProduct(pro);
          this.getTotal();
        }
      },
      {
        text: 'No',
        role: 'cancel'
      }]
    });
    if (pro.amount === 1) {
      await alert.present();
    } else {
      this.cartService.decreaseProduct(pro);
      this.getTotal();
    }
  }

  async removeProduct(pro: Product) {
    const alert = await this.alertCtrl.create({
      message: 'are you sure want remove this product',
      mode: 'ios',
      buttons: [{
        text: 'yes',
        handler: () => {
          this.cartService.removeProduct(pro);
          this.getTotal();
        }
      },
      {
        text: 'No',
        role: 'cancel'
      }]
    });
    await alert.present();
  }


  getTotal() {
    if (this.cart.length > 0) {
      this.cart.forEach((pro) => {
        return this.totalCash = this.cart.reduce((i, j) => i + j.amount * j.price, 0);
      });
    } else {
      return this.totalCash = 0;
    }
  }

  conformOrder() {
    this.authService.iuser.pipe(take(1)).subscribe(user => {
      if (user) {
        this.navCtrl.navigateForward('confirm');
      }
      if (!user) {
        this.toLogin();
      }
    });
  }

  toLogin() {
    this.navCtrl.navigateForward('/menu/pages/login').then(async _ => {
      const toast = await this.toastCtrl.create({
        message: 'please, login first',
        mode: 'ios',
        duration: 2000
      });
      await toast.present();
    });
  }


  track(index: number, pro: Product) {
    return pro.id;
  }


}
