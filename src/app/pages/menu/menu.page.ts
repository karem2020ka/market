import { Component, OnInit } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';
import { AuthService } from '../../service/auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  constructor(
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private auth: AuthService
  ) { }

  isUser = false;
  userData = null;


  pages = [
    {
      name: 'Home',
      icon: 'home',
      url: '/menu/pages/home'
    },
    {
      name: 'Hot Offers',
      icon: 'flame-outline',
      url: '/menu/pages/hot-offesr'
    },
    {
      name: 'Search',
      icon: 'search-outline',
      url: '/menu/pages/search'
    },
    {
      name: 'About',
      icon: 'alert-circle-outline',
      url: '/menu/pages/about'
    },
  ];

  ngOnInit() {
    // this.auth.reloadUser()
    this.auth.iuser.subscribe(user => {
      if (user) {
        this.isUser = true;
        this.auth.userId = user.uid;
        this.userData = user;

      } else {
        this.isUser = false;
        this.auth.userId = '';
      }
    });
  }



  async rateUS() {
    const alert = await this.alertCtrl.create({
      message: 'Do you like using our online Market',
      mode: 'ios',
      buttons: [{
        text: 'Not Really',
        role: 'cancel'
      },
      {
        text: 'yes!',
        handler: () => {
          console.log('it love it');
        }
      }]
    });
    await alert.present();
  }


  share() { }


  async logOut() {

    const alert = await this.alertCtrl.create({
      message: 'Are you sure to log out ',
      mode: 'ios',
      buttons: [{
        text: 'No',
        role: 'cancel'
      },
      {
        text: 'yes',
        handler: async () => {

          // logout
          this.auth.logOut();

          // toast to logout
          const toast = await this.toastCtrl.create({
            message: 'Now you are not user',
            mode: 'ios',
            duration: 3000
          });
          await toast.present();
        }
      }]
    });
    await alert.present();
  }


}
