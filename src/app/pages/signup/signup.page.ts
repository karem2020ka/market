import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActionSheetController, ToastController, NavController } from '@ionic/angular';
import { Users } from 'src/app/interfaces/users';
import { AuthService } from '../../service/auth.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  signUpForm: FormGroup;
  picUrl = '';
  pic = false;
  photos: any = [];

  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private toastCtrl: ToastController,
    private actionSheet: ActionSheetController,
    private navCtrl: NavController,
    private camera: Camera,
    private fireStorage: AngularFireStorage

  ) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.signUpForm = this.fb.group({
      userName: ['', Validators.required],
      phone: ['', Validators.required],
      email: ['', Validators.email],
      password: ['', Validators.minLength(6)],
      pasawordConfirm: ['', Validators.required]
    });
  }



  async addPicture() {
    const actionSheet = await this.actionSheet.create({
      header: 'Select Option',
      mode: 'ios',
      buttons: [
        {
          text: 'open camera',
          icon: 'camera',
          handler: () => {
            this.camera.getPicture({
              quality: 100,
              mediaType: this.camera.MediaType.PICTURE,
              encodingType: this.camera.EncodingType.JPEG,
              sourceType: this.camera.PictureSourceType.CAMERA,
              destinationType: this.camera.DestinationType.DATA_URL
            }
            ).then((img) => {

              this.picUrl = `data:image/jpeg;base64,${img}`;

              this.pic = true;
            }).catch(err => alert(err));
          }
        },
        {
          text: 'open gallary',
          icon: 'image',
          handler: () => {
            this.camera.getPicture({
              destinationType: this.camera.DestinationType.DATA_URL,
              mediaType: this.camera.MediaType.PICTURE,
              sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
            }).then((img) => {
              this.picUrl = `data:image/jpeg;base64,${img}`;
              this.pic = true;
            });
          }
        }
        ,
        {
          text: 'Cancel',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });

    await actionSheet.present();
  }


  submitSignUp() {
    const user = {
      userName: this.signUpForm.value.userName,
      phone: this.signUpForm.value.phone,
      email: this.signUpForm.value.email,
      password: this.signUpForm.value.password,
      pasawordConfirm: this.signUpForm.value.pasawordConfirm
    };

    if (user.pasawordConfirm === user.password) {
      this.auth.signUpWithEmail(user.email, user.password)
        .then(res => {
          this.auth.takePhoto(user.userName, this.picUrl, res.user.uid);
        })
        .then(async _ => {
          const toast = await this.toastCtrl.create({
            message: 'sing up successed',
            duration: 2000
          });
          await toast.present();
        })
        .catch(async err => {
          const toast = await this.toastCtrl.create({
            message: err,
            duration: 2000
          });
          await toast.present();
        });
    }
    this.navCtrl.navigateForward('/menu/pages/home');
  }



}
