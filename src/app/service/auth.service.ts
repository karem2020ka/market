import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Storage } from '@ionic/storage';
import * as firebase from 'firebase/app';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { Observable } from 'rxjs';
import { Users } from '../interfaces/users';
import { ToastController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';



const userinfo = 'userInfo';
@Injectable({
  providedIn: 'root'
})

export class AuthService {

  user: Users;
  userData = null;
  iuser: Observable<firebase.default.User>;
  userId = '';

  constructor(
    private auth: AngularFireAuth,
    private firestor: AngularFirestore,
    private storage: Storage,
    public facebook: Facebook,
    private toastCtrl: ToastController,
    private fireStorage: AngularFireStorage
  ) {
    this.iuser = auth.user;
  }

  signUpWithEmail(email: string, password: string) {
    return this.auth.createUserWithEmailAndPassword(email, password);
  }



  loginWithEmail(email: string, password: string) {
    return this.auth.signInWithEmailAndPassword(email, password);
  }


  signFaceBook() {
    this.facebook.login(['public_profile', 'email'])
      .then((response: FacebookLoginResponse) => {
        const crediential = firebase.default.auth.FacebookAuthProvider.credential((response.authResponse.accessToken));
        firebase.default.auth().signInWithCredential(crediential);

        this.facebook
          .api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', [])
          .then(profile => {
            this.auth.user.subscribe(user => {
              user.updateProfile({
                displayName: profile.name,
                photoURL: profile.picture_large.data.url
              });
            });
          })
          .then(async () => {
            const toast = await this.toastCtrl.create({
              message: `login success, hi ${this.userData.displayName}`,
              duration: 3000
            });
            await toast.present();
          });
      })
      .catch(async err => {
        const toast = await this.toastCtrl.create({
          message: err,
          duration: 3000
        });
        await toast.present();
      });

  }

  logOut() {
    return this.auth.signOut();
  }

  takePhoto(name: string, image: string, userId: string) {
    const ref = this.fireStorage.ref('users/' + userId);
    // uplade photo to storage
    ref.putString(image, 'data_url')
      .then(_ => {
        // update user data with photo and display name
        ref.getDownloadURL().subscribe(photoUrl => {

          this.iuser.subscribe(u => {
            u.updateProfile({
              displayName: name,
              photoURL: photoUrl
            });
          });
        });
      })
      .catch(err => alert(err));
  }



  // addUser(id: string, photoUrl: string, email: string, userName: string,) {
  //   // let data = { id, userName, phone }

  //   let user = { id, photoUrl, email, userName };
  //   this.user = user == null ? {} : user
  //   return this.storage.set(userinfo, this.user)

  // }

  // async reloadUser() {
  //   let user = await this.storage.get(userinfo);
  //   this.user = user == null ? {} : user;
  // }


}


