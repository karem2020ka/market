import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Product } from '../interfaces/product';
import { Category } from '../interfaces/category';
import { Subcategory } from '../interfaces/subcategory';
import { map } from '../../../node_modules/rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private cart = [];
  private navparams: any = [];

  constructor(
    private afs: AngularFirestore
  ) { }

  getparams() {
    return this.navparams;
  }

  setparams(body: any) {
    this.navparams = [body];

  }

  addProduct(product: any) {
    this.cart.push(product);
  }

  getCategory() {
    return this.afs
      .collection<Category>('category', (ref) => ref.orderBy('ord'))
      .snapshotChanges()
      .pipe(
        map((actions) => {
          return actions.map((element) => {
            const id = element.payload.doc.id;
            const data = element.payload.doc.data() as Category;
            return { id, ...data };
          });
        })
      );
  }

  getSubCategory(catId: string) {
    return this.afs
      .collection<Subcategory>('subCategory', (ref) =>
        ref.where('categoryId', '==', catId)
      )
      .snapshotChanges().pipe(
        map((actions) => {
          return actions.map((element) => {
            const id = element.payload.doc.id;
            const data = element.payload.doc.data() as Subcategory;
            return { id, ...data };
          });
        })
      );
  }

  getProductWithCategory(catId: string) {
    return this.afs
      .collection<Product>('products', (ref) =>
        ref.where('catId', '==', catId).orderBy('name').limit(6)
      )
      .snapshotChanges().pipe(
        map((actions) => {
          return actions.map((element) => {
            const id = element.payload.doc.id;
            const data = element.payload.doc.data() as Product;
            return { id, ...data };
          });
        })
      );
  }

  getProductWithSebCategory(subId: string) {
    return this.afs
      .collection<Product>('products', (ref) =>
        ref.where('subId', '==', subId).limit(4)
      )
      .snapshotChanges().pipe(
        map((actions) => {
          return actions.map((ele) => {
            const id = ele.payload.doc.id;
            const data = ele.payload.doc.data() as Product;
            return { id, ...data };
          });
        })
      );
  }

  /***********************    [   Discuont Products  ]    **************************/
  getDiscountProduct() {
    return this.afs.collection<Product>('products', (ref) => ref.orderBy('discount'))
      .snapshotChanges().pipe(map((action) => {
        return action.map((ele => {
          const id = ele.payload.doc.id;
          const data = ele.payload.doc.data() as Product;
          return { id, ...data };
        }));
      })
      );
  }

  /***********************    [   Pagination  ]    **************************/
  paginationCategory(lastProduct: any, Id: any) {
    return this.afs.collection<Product>('products', ref =>
      ref.where('catId', '==', Id).orderBy('name').limit(6).startAfter(lastProduct))
      .snapshotChanges().pipe(map((action) => {
        return action.map((ele => {
          const id = ele.payload.doc.id;
          const data = ele.payload.doc.data() as Product;
          return { id, ...data };
        }));
      }));
  }

  paginationSubCategory(lastProduct: any, Id: any) {
    return this.afs.collection<Product>('products', ref =>
      ref.where('subId', '==', Id).orderBy('name').limit(4).startAfter(lastProduct))
      .snapshotChanges().pipe(map((action) => {
        return action.map((ele => {
          const id = ele.payload.doc.id;
          const data = ele.payload.doc.data() as Product;
          return { id, ...data };
        }));
      }));
  }

  /***********************    [   search  ]    **************************/
  search(lowerCase: string) {
    return this.afs
      .collection<Product>('products', (ref) =>
        ref.orderBy('name').startAt(lowerCase).endAt(lowerCase + '\uf8ff')
      )
      .snapshotChanges()
      .pipe(
        map((action) => {
          console.log(action);
          return action.map((ele => {
            const id = ele.payload.doc.id;
            const data = ele.payload.doc.data() as Product;
            return { id, ...data };
          }));
        })
      );
  }













}
