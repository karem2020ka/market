import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Product } from '../interfaces/product';
import { Storage } from '@ionic/storage';
import { Address } from '../interfaces/address';

const CART = 'Cart';
const FAV = 'fav';
const ADDRESS = 'address';
@Injectable({
  providedIn: 'root'
})
export class CartService {

  // data: Product[] = [];
  public cartItemCount = new BehaviorSubject(0);
  public favCount = new BehaviorSubject(0);
  cart = [];
  fav = [];
  address = [];

  totalCash: number;
  totalAmount: number;

  constructor(
    private storage: Storage
  ) { }



  // getProduct() {
  //   return this.data;
  // }

  getCart() {
    // const nowcart = this.storage.get(CART);     // CHECK If i need this line or not
    // this.cart.concat(nowcart);                 // CHECK If i need this line or not
    return this.cart;
  }

  getFavorite() {
    return this.fav;
  }

  // getAddress() {
  //   const nowAddress = this.storage.get('address');
  //   this.address.concat(nowAddress);
  //   return this.address;
  // }

  getCartCount() {
    return this.cartItemCount.asObservable();
  }

  getFavCount() {
    return this.favCount.asObservable();
  }


  /**********************     [ add and decrease and remove product from cart ]     ********************/
  addProduct(product: Product) {
    let added = false;
    if (!product.amount) {
      product.amount = 1;
    }
    for (const p of this.cart) {
      if (p.id === product.id) {
        added = true;
        p.amount += 1;
        break;
      }
    }
    if (!added) {
      this.cart.push(product);
    }
    this.cartItemCount.next(this.cartItemCount.value + 1);
    return this.storage.set(CART, this.cart);
  }

  decreaseProduct(product: Product) {
    for (const [index, p] of this.cart.entries()) {
      if (p.id === product.id) {
        p.amount -= 1;
        if (p.amount === 0) {
          this.cart.splice(index, 1);
        }
      }
    }
    this.cartItemCount.next(this.cartItemCount.value - 1);
    return this.storage.set(CART, this.cart);
  }

  removeProduct(product: Product) {
    for (const [index, p] of this.cart.entries()) {
      if (p.id === product.id) {
        this.cartItemCount.next(this.cartItemCount.value - p.amount);
        this.cart.splice(index, 1);
        return this.storage.set(CART, this.cart);
      }
    }
  }

  /****************************         [ get data from storage ]         ***************************/
  async reloadDataStorage(key: string) {
    if (key === 'cart') {
      const cart = await this.storage.get(CART);
      this.cart = cart == null ? [] : cart;
    }
    if (key === 'fav') {
      const fav = await this.storage.get(FAV);
      this.fav = fav == null ? [] : fav;
    }
    if (key === 'address') {
      const address = await this.storage.get(ADDRESS);
      this.address = address == null ? [] : address;
    }
  }

  /******************************         [ add to favorite or remove it ]           ***************************/
  toggleFavorite(pro: Product) {
    let added = false;
    for (const [index, p] of this.fav.entries()) {
      if (p.id === pro.id) {
        added = true;
        this.fav.splice(index, 1);
        this.favCount.next(this.favCount.value - 1);
        pro.favorite = false;
      }
    }
    if (!added) {
      this.fav.push(pro);
      this.favCount.next(this.favCount.value + 1);
      pro.favorite = true;
    }
    return this.storage.set(FAV, this.fav);
  }

  /********************************       [ Address ]         ******************************/

  addAddress(address: Address) {
    this.address.push(address);
    return this.storage.set(ADDRESS, this.address);
  }

  deleteAddress(index: number) {
    this.address.splice(index, 1);
    return this.storage.set(ADDRESS, this.address);
  }

  updateAddress(address: Address, index: number) {
    this.address[index] = address;
    return this.storage.set(ADDRESS, this.address);
  }














}
