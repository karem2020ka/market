export interface Product {
  id?: string;
  name?: string;
  price?: number;
  description?: string;
  imgUrl?: string;
  catId?: string;
  subId?: string;
  amount?: number;
  favorite?: boolean;
  discount?: number;
}
