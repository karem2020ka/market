export interface Category {
    id?: string;
    name?: string;
    imgUrl?: string;
    sub?: boolean;
    color?: string;
}
