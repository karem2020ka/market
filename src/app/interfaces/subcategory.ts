export interface Subcategory {
    id?: string;
    name?: string;
    catId?: string;
}
