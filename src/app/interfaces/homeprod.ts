import { Category } from './category';
import { Product } from './product';
export interface Homeprod {
    category: Category;
    products: Product[];
}
