
export interface Address {
  country: string;
  city: string;
  state: string;
  address: string;
  pincode: number;
}
