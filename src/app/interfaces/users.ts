export interface Users {
  id?: string;
  userName?: string;
  email?: string;
  password?: string;
  passwordConfirm?: string;
  phone?: number;
  photoUrl: string;
}
